#define MaxSize 20
#define Elemtype int
typedef struct {
	Elemtype data[MaxSize];
	int top;
}SqStack;

void InitStack(SqStack &S) {
	S.top = -1;//没有值的时候让它等于-1.如果这里是1了，出栈的代码和进栈的代码以及检测是否为空栈的代码有什么区别
}

int main() {
	SqStack S;
	InitStack(S);
}
//进栈操作
bool Push(SqStack& S, Elemtype x) {
	if (S.top==MaxSize-1)
	{
		return false;
	}
	S.top = S.top + 1;
	S.data[S.top] = x;
	//S.data[++S.top]=x;//先使得top+1，让后使用这个值，区分top++
	return true;
}

//出栈操作
bool Pop(SqStack& S, Elemtype &x) {
	if (S.top ==  - 1)
	{
		return false;
	}
	x = S.data[S.top];
	S.top = S.top - 1;
	//x=S.data[S.top--];
	return true;
}

//读取栈顶操作
bool GetTop(SqStack& S, Elemtype &x) {
	if (S.top== -1)
	{
		return false;
	}
	x = S.data[S.top];
	return true;
}

//销毁栈
bool DestroyStack(SqStack& S, Elemtype& x) {
	S.top = -1;  // 将栈顶指针置为 -1
	return true;
}
//共享栈
//typedef struct {
//	Elemtype data[MaxSize];
//	int top1;
//	int top2;
//}SqStack;
//
//void InitStack(SqStack& S) {
//	S.top1 = -1;//没有值的时候让它等于-1.如果这里是1了，出栈的代码和进栈的代码以及检测是否为空栈的代码有什么区别
//	S.top2 = MaxSize;
//}
//栈满的条件
//top0+1==top1