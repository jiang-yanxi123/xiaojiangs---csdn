#include<stdio.h>
#include<stdlib.h>
//静态数组
#define MaxSize 10//定义最大长度
typedef struct {
	int data[MaxSize];
	int length;
}Sqlist;

void InitList(Sqlist& L) {
	L.length = 0;
}

bool  ListInsert(Sqlist& L, int i, int e) {//在L的位序i处插入元素e
	if (i<1 || i>L.length + 1)
		return false;
	if (L.length>=MaxSize)
		return false;
	for (int j = L.length; j >= i; j--)//将第i个元素及之后的元素往后移动
		L.data[j] = L.data[j - 1];
	L.data[i - 1] = e;
	L.length++;
	return true;
}
bool ListDelete(Sqlist& L,int i,int &e){
	if (i<1 || i>L.length + 1)
		return false;
	e = L.data[i - 1];
	for (int j = i; j < L.length;j++)
	{
		L.data[j - 1] = L.data[j];
	}
	L.length--;
	return true;
 }
//按位查找-静态数组
int GetElem(Sqlist L, int i) {
	return L.data[i - 1];
}
//按位查找-动态数组-与静态数组一样
//按值查找
int LocateElem(Sqlist L, int e) {
	for (int  i = 0; i < L.length; i++)
		if (L.data[i] == e)//匹配
			return i + 1;
	return 0;

}

int main() {
	Sqlist L;
	InitList(L);
	ListInsert(L, 3, 3);
	for (int i = 0; i < MaxSize; i++)
	{
		printf("data[%d]=%d\n", i, L.data[i]);
	}
	return 0;
}

////动态分配
//#define ElemType int
//#define Initsize 10
//typedef struct {
//	ElemType* data;//表示动态分配数组的指针
//	int Maxsize;
//	int length;
//}SeqList;
//
//void InitList(SeqList& L) {
//	//用malloc函数申请一片连续的存储空间
//	L.data = (int*)malloc(Initsize * sizeof(int));
//	L.length = 0;
//	L.Maxsize = 0;
//}
//
////增加动态数组的长度
//void IncreaseSize(SeqList& L, int len) {
//	int* p = L.data;
//	L.data= (int*)malloc((L.Maxsize+len)* sizeof(int));
//	for (int i = 0; i < L.length; i++)
//	{
//		L.data[i] = p[i];
//	}
//	L.Maxsize = L.Maxsize + len;
//	free(p);
//}
//
//int main() {
//	SeqList L;
//	InitList(L);
//	IncreaseSize(L, 5);
//	return 0;
//}