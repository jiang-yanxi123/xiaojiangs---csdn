#include<stdio.h>
#include <malloc.h>
typedef int ElemType;
typedef struct LNode{
	ElemType data;
	struct LNode* next;
}LNode,*LinkList;
//不带头结点的单链表
//bool InitList(LinkList &L){
//	L= NULL;
//	return true;
//}
//带头结点的单链表
bool InitList(LinkList& L) {
	L = (LNode*)malloc(sizeof(LNode));
	if (L==NULL)
		return false;
	L->next = NULL;
	return true;
}
//按位序插入操作 N
bool ListInsert(LinkList& L, int i, ElemType e) {
	if (i < 1)
		return false;
	LNode* p;
	int j = 0;
	p = L;
	while (p!=NULL&&j<=i-1)
	{
		p = p->next;
		j++;
	}
	if (p==NULL)
	{
		return false;
	}
	LNode* s = (LNode*)malloc(sizeof(int));
	s->data = e;
	//顺序不能错，不然就飞了
	s->next = p->next;
	p->next = s;
	return true;
}
//按位序插入操作 N 不带头结点
bool ListInsert2(LinkList& L, int i, ElemType e) {
	if (i < 1)
		return false;
	if (i == 1) {
		LNode* s = (LNode*)malloc(sizeof(LNode));
		s->data = e;
		s->next = L;
		L = s;
		return true;
	}

	LNode* p;
	int j = 1;
	p = L;
	while (p != NULL && j <= i - 1)
	{
		p = p->next;
		j++;
	}
	if (p == NULL)
	{
		return false;
	}
	//函数复用
	return InsertNextNode(p, e);
	//LNode* s = (LNode*)malloc(sizeof(int));
	//if (s = NULL)
	//{
	//	return false;
	//}
	//s->data = e;
	////顺序不能错，不然就飞了
	//s->next = p->next;
	//p->next = s;
	//return true;
}
//尾插  o1的时间复杂度
bool InsertNextNode(LNode* p, ElemType e) {
	if (p==NULL)
	{
		return false;
	}
	LNode* s = ((LNode*)malloc(sizeof(LNode)));
	if (s=NULL)
	{
		return false;
	}
	s->data = e;
	s->next = p->next;
	p->next = s;
	return true;
}

//前插入
bool InsertPriorNode(LNode* p, ElemType e) {
	if (p == NULL)
	{
		return false;
	}
	LNode* s = ((LNode*)malloc(sizeof(LNode)));
	if (s = NULL)
	{
		return false;
	}
	s->next = p->next;
	p->next = s;
	s->data = p->data;
	p->data = e;
	return true;
}
//删除 按位序删除带头节点
bool ListDele(LinkList& L, int i, ElemType& e) {
	if (i < 1)
		return false;
	LNode* p;
	int j = 0;
	p = L;//L指向头结点，头结点是第0个节点
	while (p != NULL && j <= i - 1)//循环找到第i-1个节点
	{
		p = p->next;
		j++;
	}
	if (p == NULL)
	{
		return false;
	}
	if (p->next==NULL)
	{
		return false;
	}
	LNode* q = p->next;
	e = q->data;
	p->next = q->next;
	free(q);
	return true;
}
//删除指定结点p
bool DeleteNode(LNode* p) {
	if (p==NULL)
	{
		return false;
	}
	LNode* q = p->next;
	p->data = p->next->data;
	p->next = q->next;
	free(q);
	return true;
}
void test() {
	LinkList L;
	InitList(L);
}