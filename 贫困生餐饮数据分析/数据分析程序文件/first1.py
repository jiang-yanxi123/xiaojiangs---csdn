import pandas as pd
# 读取三个Excel文件
df1 = pd.read_excel('附件1第一年三餐消费数据.xlsx')
df2 = pd.read_excel('附件2第二年三餐消费数据.xlsx')
df3 = pd.read_excel('附件3第三年三餐消费数据.xlsx')
# 合并三个 DataFrame 对象
df_all = pd.concat([df1, df2, df3], ignore_index=True)
# 按序号分组，求和
result = df_all.groupby(['序号']).sum()
# 重置索引
result = result.reset_index()
# 将结果输出到新文件
result.to_excel('firstSum.xlsx', index=False)
# 检查缺失值
print(result.isnull().sum())
