import pandas as pd

# 读取 Excel 文件
df = pd.read_excel('firstSum.xlsx', header=0)

# 排序（按照序号升序）
df = df.sort_values(by=['序号'])

# 计算每行的消费信息
consumption_data = []
for idx, row in df.iterrows():
    data = list(row[1:])
    count = data.count(0)
    data = [val for val in data if val != 0]
    consumption_data.append({
        '序号': row['序号'],
        'max': max(data) if len(data) > 0 else '',
        'min': min(data) if len(data) > 0 else '',
        'median': pd.Series(data).median() if len(data) > 0 else '',
        'mean': pd.Series(data).mean() if len(data) > 0 else '',
        'variance': pd.Series(data).var(ddof=0) if len(data) > 0 else '',
        'count': len(data),
        'unconsumed': count
    })

# 将结果写入新的 Excel 文件
wb = pd.ExcelWriter('ClearfirstSum.xlsx')
pd.DataFrame(consumption_data).to_excel(wb, index=False)
wb.save()
