import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score

# 读取Excel文件
data = pd.read_excel('origindata.xlsx')

# 提取"mean_min-max"标准化列数据
normalized_data = data['mean_min-max标准化'].values.reshape(-1, 1)

# 使用K均值聚类将数据分为3个簇
kmeans = KMeans(n_clusters=3)
kmeans.fit(normalized_data)

# 获取每个数据点的簇索引
cluster_labels = kmeans.labels_

# 计算轮廓系数、DBI和CH指数
silhouette_avg = silhouette_score(normalized_data, cluster_labels)
dbi_score = davies_bouldin_score(normalized_data, cluster_labels)
ch_score = calinski_harabasz_score(normalized_data, cluster_labels)

# 将簇索引和轮廓系数添加到数据框中
data['Cluster'] = cluster_labels

# 生成新的Excel文件
data.to_excel('newdata1.xlsx', index_label='Index')

# 绘制散点图
plt.scatter(data.index, normalized_data, c=cluster_labels, cmap='viridis')
plt.title('Clustered Data')
plt.xlabel('Index')
plt.ylabel('mean_min-max')
plt.show()

# 显示轮廓系数、DBI和CH指数
print('Silhouette Score:', silhouette_avg)
print('DBI Score:', dbi_score)
print('CH Score:', ch_score)
