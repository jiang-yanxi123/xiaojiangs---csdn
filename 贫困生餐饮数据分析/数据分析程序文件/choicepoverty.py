import pandas as pd

# 读取Excel文件
data = pd.read_excel('newdata.xlsx')

# 利用Cluster的值进行贫困等级分类
data['Poverty Level'] = data['Cluster'].map({0: '特别贫困', 1: '一般贫困', 2: '不贫困'})

# 生成新的Excel文件
data.to_excel('newdata_with_poverty_level.xlsx', index=False)

# 打印结果
print(data[['Index', 'Cluster', 'Poverty Level']])