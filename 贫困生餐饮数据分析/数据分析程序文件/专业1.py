import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score

# 读取附件8中的标签信息贫困程度.xlsx
labels_data = pd.read_excel('附件8 已知贫困标签.xlsx')
labels = labels_data['贫困程度']

# 读取附件9中的待补全标签数据.xlsx
incomplete_data = pd.read_excel('附件9 问题2待补全标签数据.xlsx')

# 使用MinMaxScaler进行数据的归一化处理
scaler = MinMaxScaler()
normalized_labels = scaler.fit_transform(labels.values.reshape(-1, 1))

# 特征选择，这里选择了 "max_min-max标准化"、"min_min-max标准化"、"median_min-max标准化"、"mean_min-max标准化"、"variance_min-max标准化" 和 "count_min-max标准化"
features = labels_data[['max_min-max标准化', 'min_min-max标准化', 'median_min-max标准化', 'mean_min-max标准化', 'variance_min-max标准化', 'count_min-max标准化']]

# 将数据分割为训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(features, normalized_labels, test_size=0.3, random_state=42)

# 创建决策树回归模型并进行训练
model = DecisionTreeRegressor()
model.fit(X_train, y_train)

# 在测试集上进行预测
predicted_labels = model.predict(X_test)

# 反归一化得到原始贫困程度的预测值
predicted_labels = scaler.inverse_transform(predicted_labels.reshape(-1, 1))

# 计算评估指标
accuracy = accuracy_score(scaler.inverse_transform(y_test), predicted_labels)
recall = recall_score(scaler.inverse_transform(y_test), predicted_labels, average='macro')
precision = precision_score(scaler.inverse_transform(y_test), predicted_labels, average='macro')
f1 = f1_score(scaler.inverse_transform(y_test), predicted_labels, average='macro')

print("准确率:", accuracy)
print("召回率:", recall)
print("精确率:", precision)
print("F1值:", f1)
