#pragma once
#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>
#include<assert.h>

typedef int STDataType;

typedef struct  Stack 
{
	STDataType* a;
	int top;
	int capacity;
}ST;

void STInit(ST* ps);//初始化
void STDesroy(ST* ps);//销毁
void STPush(ST* ps,STDataType x);//插入
void STPop(ST* ps);//删除

int STSize(ST* ps);//数据的长度
bool STEmpty(ST* ps);//判断是否为空值
STDataType STTop(ST* ps);//取栈顶的数据

