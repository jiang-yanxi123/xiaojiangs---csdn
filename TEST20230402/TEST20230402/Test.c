#include"Stack.h"

void TestStack()
{
	ST st;
	STInit(&st);
	STPush(&st,1);
	STPush(&st, 2);
	printf("%d ", STTop(&st));
	STPop(&st);
	STPush(&st, 3);
	STPush(&st, 4);
	printf("%d ", STTop(&st));
	STPop(&st);
	STPush(&st, 5);
	while (!STEmpty(&st))
	{
		printf("%d ", STTop(&st));
		STPop(&st);
	}
	STDesroy(&st);
}

int main() 
{
	TestStack();
	return 0;
}