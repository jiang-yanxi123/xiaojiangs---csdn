#include"Stack.h"

void STInit(ST* ps)//初始化
{
	assert(ps);
	ps->a =(STDataType*)malloc(sizeof(STDataType)*4);
	if (ps->a == NULL) {
		printf("malloc fail\n");
		return;
	}
	ps->capacity = 4;
	ps->top = 0;//ps->top = 1;
	//top=0，意味着top指向栈顶数据的下一个，top=-1指向的是栈顶数据
}

void STDesroy(ST* ps)//销毁
{
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}

void STPush(ST* ps, STDataType x)//插入
{
	assert(ps);
	//判断空间是否满的
	if (ps->top == ps->capacity) {
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) *ps->capacity*2);
		if (tmp==NULL) {
			printf("relloc fail\n");
			return;
		}
		//把数据放到top的位置
		ps->a = tmp;
		ps->capacity*=2;
	}
	ps->a[ps->top] = x;
	ps->top++;
}

void STPop(ST* ps)//删除
{
	assert(ps);
	assert(!STEmpty(ps));
	ps->top--;
}

int STSize(ST* ps) {
	assert(ps);
	return ps->top;

}

bool STEmpty(ST* ps)//判断是否为空值
{
	assert(ps);
	return ps->top == 0;

}

STDataType STTop(ST* ps)//取栈顶的数据
{
	assert(ps);
	assert(!STEmpty(ps));//加断言解释一下
	return ps->a[ps->top - 1];
}

