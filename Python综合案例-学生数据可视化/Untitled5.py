#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

df = pd.read_csv('StudentPerformance.csv')

print(df.head())


# In[2]:


import pandas as pd

df = pd.read_csv('StudentPerformance.csv')

# 新列名列表
new_columns = ['性别', '民族', '出生地', '学段', '年级', '班级', '主题', '学期', '与家长关系', '举手次数', '浏览课件查看次数', '公告查看次数', '参与讨论次数', '家长是否回答调查问卷', '家长对学校满意度', '学生缺勤天数', '班级']

# 将原列名与新列名对应起来，组成字典
rename_dict = dict(zip(df.columns, new_columns))

# 使用rename()函数修改列名
df.rename(columns=rename_dict, inplace=True)

print(df.head())


# In[10]:


import pandas as pd

df = pd.read_csv('StudentPerformance.csv')

# 显示学期的取值
print(df['Semester'].unique())

# 显示学段的取值
print(df['StageID'].unique())


# In[12]:


import pandas as pd

df = pd.read_csv('StudentPerformance.csv')

# 修改学段数据
df.loc[:, 'StageID'] = df['StageID'].replace({'lowerlevel': '小学', 'middleschool': '初中', 'highschool': '高中'})

# 修改性别数据
df.loc[:, 'gender'] = df['gender'].replace({'M': '男', 'F': '女'})

# 修改学期数据
df.loc[:, 'Semester'] = df['Semester'].replace({'S': '春季', 'F': '秋季'})

print(df.head())


# In[13]:


# 检查每个单元格是否为空
null_counts = df.isnull().sum()
# 显示每列中空缺值的数量
print(null_counts)


# In[21]:


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# 读取数据并选择需要分析的列
df = pd.read_csv('StudentPerformance.csv')
scores = df['Class']

# 调整图表样式
sns.set_style('darkgrid')
sns.set_palette('pastel')

# 绘制计数柱状图
plt.figure(figsize=(8, 6))
sns.histplot(data=df, x='Class', kde=False, bins=10, alpha=0.8)

# 添加标签和标题
plt.xlabel('Total Score')
plt.ylabel('Number of Students')
plt.title('Distribution of Student Scores')

# 显示图表
plt.show()


# In[25]:


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 绘制计数柱状图
sns.countplot(data=df, x='gender')
plt.xlabel('Gender')
plt.ylabel('Count')
plt.title('Number of students by gender')

# 展示图形
plt.show()


# In[31]:


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 绘制计数柱状图
sns.countplot(data=df, x='gender', palette='husl', edgecolor='black')
plt.xlabel('Gender')
plt.ylabel('Count')
plt.title('Number of students by gender')

# 展示图形
plt.show()


# In[33]:


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 绘制计数柱状图
sns.countplot(data=df, x='Topic')
plt.xlabel('Subject')
plt.ylabel('Count')
plt.title('Number of students by subject')

# 设置x轴标签旋转角度和对齐方式
plt.xticks(rotation=45, ha='right')

# 展示图形
plt.show()


# In[43]:


import pandas as pd
import matplotlib.pyplot as plt

# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 选择需要的列和科目
cols = ['Topic', 'Class']
topic_cols = df[cols]

# 将不同成绩分组并统计数量
counts_df = topic_cols.groupby(['Topic', 'Class']).size().reset_index(name='counts')

# 创建一个包含 4 行 3 列子图的数组
fig, axes = plt.subplots(nrows=4, ncols=3, figsize=(18, 24))

# 创建一个包含所有科目的列表
subjects = ['IT', 'French', 'Arabic', 'Science', 'English', 'Biology', 'Spanish', 'Chemistry', 'Geology', 'Quran', 'Math', 'History']

# 在每个子图中绘制对应的计数柱状图
for i, subject in enumerate(subjects):
    # 获取当前科目的数据
    subject_df = counts_df[counts_df['Topic'] == subject]
    
    # 计算子图的行和列索引
    row_index = i // 3
    col_index = i % 3
    
    # 在当前子图中绘制计数柱状图
    axes[row_index, col_index].bar(subject_df['Class'], subject_df['counts'], width=0.5, color='green')
    axes[row_index, col_index].set_xlabel('Score')
    axes[row_index, col_index].set_ylabel('Number of Students')
    axes[row_index, col_index].set_title(f'Distribution of Scores for {subject}')
    axes[row_index, col_index].spines['top'].set_visible(False)
    axes[row_index, col_index].spines['right'].set_visible(False)
    for tick in axes[row_index, col_index].get_xticklabels():
        tick.set_rotation(0)

# 调整子图之间的距离和外边距
plt.subplots_adjust(wspace=0.3, hspace=0.5, top=0.95, bottom=0.05, left=0.05, right=0.95)

# 保存图像并显示
plt.savefig('subject_distribution.png', dpi=300)
plt.show()


# In[41]:


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 绘制计数柱状图
sns.countplot(data=df, x='gender', hue='Class')
plt.xlabel('Gender')
plt.ylabel('Count')
plt.title('Number of students by gender and grade')

# 设置图例位置
plt.legend(loc='upper right')

# 展示图形
plt.show()


# In[65]:


import pandas as pd
import seaborn as sns

# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 使用 Seaborn 绘制班级和成绩的人数分布情况
sns.countplot(x='SectionID', hue='Class', hue_order=['L', 'M', 'H'], data=df)

# 显示图像
plt.show()


# In[63]:




# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 计算每个成绩段内四个表现指标的平均值
mean_df = df.groupby('Class').mean()[['VisITedResources', 'AnnouncementsView', 'raisedhands', 'Discussion']].reset_index()

# 使用 Seaborn 绘制四个表现指标与成绩的关系
fig, axes = plt.subplots(2, 2, figsize=(14, 10))
sns.barplot(x='Class', y='VisITedResources', data=mean_df, order=['L', 'M', 'H'], ax=axes[0, 0], color='blue')
sns.barplot(x='Class', y='AnnouncementsView', data=mean_df, order=['L', 'M', 'H'], ax=axes[0, 1], color='green')
sns.barplot(x='Class', y='raisedhands', data=mean_df, order=['L', 'M', 'H'], ax=axes[1, 0], color='orange')
sns.barplot(x='Class', y='Discussion', data=mean_df, order=['L', 'M', 'H'], ax=axes[1, 1], color='red')

# 显示图像
plt.show()


# In[62]:



# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 设置图像样式
sns.set(style='darkgrid', font_scale=1.2)

# 绘制带有回归拟合线的散点图
g = sns.lmplot(x='Discussion', y='raisedhands', data=df, hue='Class', col='Class', col_wrap=3, height=5, aspect=.8, scatter_kws={'s': 100})

# 显示图像
plt.show()

#从图像中可以看到，随着讨论次数的增加，学生成绩也会变得更好。同时，不同成绩段内学生的回归拟合线也存在一定的差异


# In[61]:



# 读取数据集
df = pd.read_csv('StudentPerformance.csv')

# 选择需要分析的两个特征
hand_raised = df['raisedhands']
discussions = df['Discussion']

# 设置样式和配色方案
sns.set(style='whitegrid', palette='Paired')

# 绘制散点图
sns.scatterplot(x=hand_raised, y=discussions, s=80, color='g', alpha=0.7)

# 添加回归拟合线
sns.regplot(x=hand_raised, y=discussions, scatter=False, line_kws={'color': 'r', 'linewidth': 2})

# 修改标题和坐标轴标签的字体大小和样式
plt.title('Raised Hands vs. Discussion')
plt.xlabel('Raised Hands', fontsize=14, fontweight='bold')
plt.ylabel('Discussion', fontsize=14, fontweight='bold')

# 添加图例并调整位置
sns.scatterplot(x=hand_raised, y=discussions, label='Data')
sns.regplot(x=hand_raised, y=discussions, scatter=False, label='Regression Line')
plt.legend(['Data', 'Regression Line'], loc='upper left', bbox_to_anchor=(1, 1))


# 修改图例的字体大小和样式
leg = plt.legend(fontsize=12, fancybox=True)
leg.get_frame().set_alpha(0.5)

# 显示图像
plt.show()
#回归拟合线呈现上升趋势且散点较为密集，则说明举手次数和参加讨论次数之间存在正相关关系（即举手次数越多，参加讨论
#次数也越多）。反之，如果回归拟合线呈现下降趋势，则说明两者之间存在负相关关系（即举手次数越多，参加讨论次数越少） 


# In[68]:


# 设置Seaborn的样式和调色板
sns.set(style='white')
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# 绘制热力图
sns.heatmap(corr_matrix, annot=True, cmap=cmap)

# 添加标题和标签
plt.title('Correlation Matrix')
plt.xlabel('Features')
plt.ylabel('Features')

# 调整横纵坐标标签的角度
plt.xticks(rotation=45)
plt.yticks(rotation=0)

# 显示图像
plt.show()
#颜色越浅，表示两个特征之间的相关性越高；
#颜色越深，表示两个特征之间的相关性越低。矩阵的对角线上的元素值始终为 1.0，因为每个特征与自己完全相关。


# In[ ]:




