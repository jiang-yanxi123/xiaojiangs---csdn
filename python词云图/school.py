# import os
# from wordcloud import WordCloud
#
# # 读取文件内容
# file_path = 'school_intro.txt'
# if os.path.exists(file_path):
#     with open(file_path, 'r', encoding='utf-8') as f:
#         text = f.read()
# else:
#     print(f'文件 "{file_path}" 不存在！')
#     text = ''
#
# # 指定字体为宋体
# font_path = 'C:\\Windows\\Fonts\\simsun.ttc'  # Windows 系统下的宋体字体路径
# wc = WordCloud(font_path=font_path, width=800, height=600, background_color='white', max_words=50, max_font_size=100)
#
# # 生成词云图
# wc.generate(text)
#
# # 显示词云图
# import matplotlib.pyplot as plt
# plt.imshow(wc, interpolation='bilinear')
# plt.axis('off')
# plt.show()




# import os
# from wordcloud import WordCloud
#
# # 读取文件内容
# file_path = 'school_intro.txt'
# if os.path.exists(file_path):
#     with open(file_path, 'r', encoding='utf-8') as f:
#         text = f.read()
# else:
#     print(f'文件 "{file_path}" 不存在！')
#     text = ''
#
# # 指定字体为宋体
# font_path = 'C:\\Windows\\Fonts\\simsun.ttc'  # Windows 系统下的宋体字体路径
# wc = WordCloud(font_path=font_path, width=800, height=600, background_color='white', max_words=50, colormap='Dark2',
#                contour_width=1, contour_color='black', stopwords={'学校', '专业', '科研', '教师'},
#                min_font_size=10, max_font_size=80)
#
# # 生成词云图
# wc.generate(text)
#
# # 显示词云图
# import matplotlib.pyplot as plt
# plt.imshow(wc, interpolation='bilinear')
# plt.axis('off')
# plt.show()


# import os
# import re
# from collections import Counter
# import jieba
# from wordcloud import WordCloud
#
# # 读取文件内容
# file_path = '三国演义.txt'
# if os.path.exists(file_path):
#     with open(file_path, 'r', encoding='utf-8') as f:
#         text = f.read()
# else:
#     print(f'文件 "{file_path}" 不存在！')
#     text = ''
# # 使用正则表达式去除所有非汉字字符和空白字符
# text = re.sub(r'[^\u4e00-\u9fa5]', '', text)
# # 使用 jieba 进行分词
# seg_list = jieba.cut(text)
# words = [word for word in seg_list if len(word) >= 2 and word not in ('他们', '一个', '自己', '没有', '什么', '不肯', '此时','分付','那些', '就是', '还有', '已经','不可','此人', '如此', '一声', '原来','忽然')]
# # 使用 Counter 统计每个词出现的次数，并只保留出现频率最高的前 50 个词
# word_counts = Counter(words)
# top_n = 50
# words_top_n = word_counts.most_common(top_n)
# # 打印前 top_n 个出现频率最高的词
# for i, (word, count) in enumerate(words_top_n):
#     print(f'第 {i+1} 高频词：{word}, 出现次数：{count}')
# # 指定字体为宋体
# font_path = 'C:\\Windows\\Fonts\\simsun.ttc'  # Windows 系统下的宋体字体路径
# wc = WordCloud(font_path=font_path, width=800, height=600, background_color='white',
#                max_words=200, colormap='Dark2', contour_width=1, contour_color='black',
#                stopwords={}, min_font_size=10, max_font_size=80)
# # 生成词云图
# wc.generate_from_frequencies(word_counts)
# # 显示词云图
# import matplotlib.pyplot as plt
# plt.imshow(wc, interpolation='bilinear')
# plt.axis('off')
# plt.show()
#
import os
import re
from collections import Counter
import jieba
from wordcloud import WordCloud

# 读取文件内容
file_path = '红楼梦.txt'
if os.path.exists(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        text = f.read()
else:
    print(f'文件 "{file_path}" 不存在！')
    text = ''

# 使用正则表达式去除所有非汉字字符和空白字符
text = re.sub(r'[^\u4e00-\u9fa5]', '', text)

# 使用 jieba 进行分词
seg_list = jieba.cut(text)
words = [word for word in seg_list if len(word) >= 2 and word not in ('他们', '一个', '自己', '没有', '什么', '那些', '就是', '还有', '已经')]

# 使用 Counter 统计每个词出现的次数，并只保留出现频率最高的前 50 个词
word_counts = Counter(words)
top_n = 50
words_top_n = word_counts.most_common(top_n)

# 打印前 top_n 个出现频率最高的词
for i, (word, count) in enumerate(words_top_n):
    print(f'第 {i+1} 高频词：{word}, 出现次数：{count}')

# 指定字体为宋体
# font_path = 'SimSun.ttf'
font_path = 'C:\\Windows\\Fonts\\simsun.ttc'  # Windows 系统下的宋体字体路径
wc = WordCloud(font_path=font_path, width=800, height=600, background_color='white',
               max_words=200, colormap='Dark2', contour_width=1, contour_color='black',
               stopwords={}, min_font_size=10, max_font_size=80)

# 生成词云图
wc.generate_from_frequencies(word_counts)

# 显示词云图
import matplotlib.pyplot as plt
plt.imshow(wc, interpolation='bilinear')
plt.axis('off')
plt.show()
