#pragma once
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

void PrintSort(int* a, int n);
void InsertSort(int* a, int n );
void ShellSort(int* a, int n);
void SelectSort(int* a, int n);

void BubbleSort(int* a, int n);
void QuickSort(int* a,int left,int right);
void QuickSort1(int* a, int left, int right);
void QuickSort2(int* a, int left, int right);