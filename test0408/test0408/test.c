#define _CRT_SECURE_NO_WARNINGS 1
#include "Sort.h"
void test1() {
	int a[] = {1,8,9,10,11,12,18,16,19,20};
	int n = sizeof(a) / sizeof(a[0]);
	PrintSort(a, n);
	InsertSort(a,n);
	PrintSort(a,n);
}

void test2() {
	int a[] = { 123,88,9,10,11,12,18111,16,19,2026,91,58,58,2174,21,22,322,2548,222};
	int n = sizeof(a) / sizeof(a[0]);
	PrintSort(a, n);
	ShellSort(a, n);
	PrintSort(a, n);
}

void test3() {
	int a[] = { 123,88,9,10,11,12,18111,16,19,2026,91,58,58,2174,21,22,322,2548,222 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintSort(a, n);
	SelectSort(a, n);
	PrintSort(a, n);
}

void test4() {
	int a[] = { 1,9,9,8,10,111,12,118,16,19,20 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintSort(a, n);
	BubbleSort(a, n);
	PrintSort(a, n);
}

void test5() {
	int a[] = {1,9,9,8,10,111,12,118,16,19,20};
	int n = sizeof(a) / sizeof(a[0]);
	PrintSort(a, n);
	QuickSort(a, 0,n-1);
	PrintSort(a, n);
}



//void TestOP()
//{
//	srand(time(0));
//	const int N = 100000;
//	int* a1 = (int*)malloc(sizeof(int) * N);
//	int* a2 = (int*)malloc(sizeof(int) * N);
//	int* a3 = (int*)malloc(sizeof(int) * N);
//	int* a4 = (int*)malloc(sizeof(int) * N);
//	int* a5 = (int*)malloc(sizeof(int) * N);
//	int* a6 = (int*)malloc(sizeof(int) * N);
//	for (int i = 0; i < N; ++i)
//	{
//		a1[i] = rand();
//		a2[i] = a1[i];
//		a3[i] = a1[i];
//		a4[i] = a1[i];
//		a5[i] = a1[i];
//		a6[i] = a1[i];
//	}
//	int begin1 = clock();
//	InsertSort(a1, N);
//	int end1 = clock();
//	int begin2 = clock();
//	ShellSort(a2, N);
//	int end2 = clock();
//	int begin3 = clock();
//	SelectSort(a3, N);
//	int end3 = clock();
//	int begin4 = clock();
//	HeapSort(a4, N);
//	int end4 = clock();
//	int begin5 = clock();
//	QuickSort(a5, 0, N - 1);
//	int end5 = clock();
//	int begin6 = clock();
//	MergeSort(a6, N);
//	int end6 = clock();
//	printf("InsertSort:%d\n", end1 - begin1);
//	printf("ShellSort:%d\n", end2 - begin2);
//	printf("SelectSort:%d\n", end3 - begin3);
//	printf("HeapSort:%d\n", end4 - begin4);
//	printf("QuickSort:%d\n", end5 - begin5);
//	printf("MergeSort:%d\n", end6 - begin6);
//	free(a1);
//	free(a2);
//	free(a3);
//	free(a4);
//	free(a5);
//	free(a6);
//}

int main() {
	test5();
	return 0;
}


