#include "Sort.h"
void PrintSort(int* a, int n) {
	for ( int i = 0; i <n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}


void InsertSort(int* a, int n) {
	for (int i = 1; i < n; i++)
	{
		int end=i-1;
		int tmp=a[i];
		//将tmp插入[0,end]区间
		while (end>=0)
		{
			if (tmp < a[end])
			{
				a[end + 1] =a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}
void ShellSort(int* a, int n) {
	int gap = n;
	while (gap > 0) {
		gap /= 2;
	for (int j = 0; j < gap; j++)
		{
		for (int i = j; i <=n - gap; i += gap)
			{
				int end = i - gap;
				int tmp = a[end + gap];
				while (end >= 0)
				{
					if (tmp < a[end])
					{
						a[end + gap] = a[end];
						end -= gap;
					}
					else
					{
						break;
					}
				}
				a[end + gap] = tmp;
			}
		}
	}
}

void Swap(int*p1,int* p2) {
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}
void SelectSort(int* a, int n) {
	int left = 0, right = n - 1;
	while (left<right)
	{
		int mini = left, maxi = left;
		for (int i = 0; i < n; i++)
		{
			if (a[i]<a[mini])
			{
				mini = i;
			}
			if (a[i]>a[maxi])
			{
				maxi = i;
			}
		}
		Swap(&a[left],&a[mini]);
		if (left == maxi)
		{

		}
		++left;
		--right;
	}
}

void BubbleSort(int* a, int n) {
	for ( int i = 0; i < n; i++)
	{
		bool exchange = false;
		for(int j=0;j<n-i-1;j++)
		{ 
			if (a[j]>a[j+1])
			{
				Swap(&a[j], &a[j + 1]);
				exchange = true;
			}
		}
		if (exchange=false)
		{
			break;
		}
	}
}

int  GetMidNumi(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left]<a[mid])
	{
		if (a[mid]<a[right])
		{
			return mid;
		}
		else if (a[left]>a[right])
		{
			return left;

		}
		else
		{
			return right;
		}
	}
	else
	{
		if (a[mid]>a[right])
		{
			return mid;
		}
		else if(a[left]<a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}


void QuickSort(int* a, int left, int right){
	if (left > right)
		return;
	int begin = left, end = right;
	//随机选KEY
	//int randi = left + (rand() % (right - left));
	//Swap(&a[left], &a[randi]);
	//三数取中
	int midi = GetMidNumi(a, left, right);
	Swap( &a[midi], &a[left]);
	int keyi = left;
	while (left < right)
	{//右边找小
		while (left < right && a[right] >= a[keyi])
			--right;
	//左边找大
		while (left < right && a[left] <= a[keyi])
			++left;
		Swap(&a[left], &a[right]);
	}
	Swap(&a[keyi], &a[left]);
	keyi = left;
	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1, end);
}

void QuickSort1(int* a, int left, int right) {
	if (left > right)
		return;
	int begin = left, end = right;
	//随机选KEY
	//int randi = left + (rand() % (right - left));
	//Swap(&a[left], &a[randi]);
	//三数取中
	int midi = GetMidNumi(a, left, right);
	Swap(&a[midi], &a[left]);
	int key =a[left];
	int hole = left;
	while (left < right)
	{//右边找小
		while (left < right && a[right] >= key)
			--right;
		//左边找大
		a[hole] = a[right];
		hole = right;
		while (left < right && a[left] <= key)
			++left;
		a[hole] = a[left];
		hole = left;
	}
	a[hole] = key;
	QuickSort1(a, begin, hole - 1);
	QuickSort1(a, hole + 1, end);
}

void QuickSort2(int* a, int left, int right) {
	int midi = GetMidNumi(a, left, right);
	Swap(&a[midi], &a[left]);
	int keyi = left;
	int prev = left;
	int cur= left + 1;
	while (cur <= right) {
		for (int i = 0; i < right; i++)
		{
			if (a[cur]<a[keyi] && ++prev!=cur)
			{
				Swap(&a[cur], &a[prev]);
				++cur;
			}
	}
		Swap(&a[cur],&a[keyi]);
		keyi = prev;
		return keyi;
}

