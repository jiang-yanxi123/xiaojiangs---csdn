#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

data = pd.read_csv("directory.csv")
print(data.head(10))  # 显示数据框的前十行数据


# In[2]:


import pandas as pd

# 读取CSV文件并查看前5行数据
data = pd.read_csv("directory.csv")
print(data.head())

# 查看品牌种类
brands = data["Brand"].unique()
print("Starbucks旗下的品牌有：")
for brand in brands:
    print(brand)

# 统计星巴克门店数量
starbucks_stores = data[data["Brand"] == "Starbucks"]
num_starbucks_stores = len(starbucks_stores)
print(f"\n全球一共有{num_starbucks_stores}家星巴克咖啡门店。")


# In[3]:


import pandas as pd

# 读取CSV文件并筛选出品牌为 Starbucks 的行
starbucks = pd.read_csv("directory.csv")
starbucks = starbucks[starbucks["Brand"] == "Starbucks"]

# 统计全球门店数量
num_stores_global = len(starbucks)
print(f"全球一共有 {num_stores_global} 家星巴克咖啡门店")

# 按国家分组并统计门店数量，按门店数量排序
stores_by_country = starbucks.groupby("Country").size().reset_index(name="NumStores")
stores_by_country = stores_by_country.sort_values(by="NumStores", ascending=False).reset_index(drop=True)

# 显示门店数量排名前10的国家和地区
top10_countries = stores_by_country.head(10)
print("\n门店数量排名前10的国家和地区：")
for i, row in top10_countries.iterrows():
    print(f"{i+1}. {row['Country']}: {row['NumStores']} 家门店")

# 显示门店数量排名后10的国家和地区
bottom10_countries = stores_by_country.tail(10).sort_values(by="NumStores")
print("\n门店数量排名后10的国家和地区：")
for i, row in bottom10_countries.iterrows():
    print(f"{len(stores_by_country)-9+i}. {row['Country']}: {row['NumStores']} 家门店")


# In[7]:


import pandas as pd
import matplotlib.pyplot as plt

# 读取 CSV 文件并选择品牌为 Starbucks 的行
starbucks = pd.read_csv("directory.csv")
starbucks = starbucks[starbucks["Brand"] == "Starbucks"]

# 将数据按国家分组，计算每个国家的店铺数量，并按数量降序排列
stores_by_country = starbucks.groupby("Country").size().reset_index(name="NumStores")
stores_by_country = stores_by_country.sort_values(by="NumStores", ascending=False).reset_index(drop=True)

# 选择数量最多的前 10 个国家
top10_countries = stores_by_country.head(10)

# 创建柱状图
plt.figure(figsize=(10, 6))
plt.bar(top10_countries["Country"], top10_countries["NumStores"], color="blue", alpha=0.8)
plt.title("Top 10 Countries with Most Starbucks Stores", fontsize=16)
plt.xlabel("Country", fontsize=14)
plt.ylabel("Number of Stores", fontsize=14)
plt.xticks(rotation=30)
plt.tight_layout()
plt.show()


# In[8]:


import pandas as pd
import matplotlib.pyplot as plt

# 读取 CSV 文件并选择品牌为 Starbucks 的行
starbucks = pd.read_csv("directory.csv")
starbucks = starbucks[starbucks["Brand"] == "Starbucks"]

# 将数据按国家分组，计算每个国家的店铺数量，并按数量降序排列
stores_by_country = starbucks.groupby("Country").size().reset_index(name="NumStores")
stores_by_country = stores_by_country.sort_values(by="NumStores", ascending=False).reset_index(drop=True)

# 选择数量最少的前 10 个国家
bottom10_countries = stores_by_country.tail(10)

# 创建柱状图
plt.figure(figsize=(10, 6))
plt.bar(bottom10_countries["Country"], bottom10_countries["NumStores"], color="red", alpha=0.8)
plt.title("Bottom 10 Countries with Least Starbucks Stores", fontsize=16)
plt.xlabel("Country", fontsize=14)
plt.ylabel("Number of Stores", fontsize=14)
plt.xticks(rotation=30)
plt.tight_layout()
plt.show()


# In[10]:


import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif']=['SimSun'] # 将默认字体设置为宋体

# 接下来的绘图代码


# In[11]:


import pandas as pd
import matplotlib.pyplot as plt

# 读取 CSV 文件并选择品牌为 Starbucks 的行
starbucks = pd.read_csv("directory.csv")
starbucks = starbucks[starbucks["Brand"] == "Starbucks"]

# 将数据按城市分组，计算每个城市的店铺数量，并按数量降序排列
stores_by_city = starbucks.groupby("City").size().reset_index(name="NumStores")
stores_by_city = stores_by_city.sort_values(by="NumStores", ascending=False).reset_index(drop=True)

# 选择数量最多的前 10 个城市
top10_cities = stores_by_city.head(10)

# 创建条形图
plt.figure(figsize=(10, 6))
plt.barh(top10_cities["City"], top10_cities["NumStores"], color="green", alpha=0.8)
plt.title("Top 10 Cities with Most Starbucks Stores", fontsize=16)
plt.xlabel("Number of Stores", fontsize=14)
plt.ylabel("City", fontsize=14)
plt.tight_layout()
plt.show()


# In[12]:


import pandas as pd
import matplotlib.pyplot as plt

# 读取 CSV 文件并选择品牌为 Starbucks 的行
starbucks = pd.read_csv("directory.csv")
starbucks = starbucks[starbucks["Brand"] == "Starbucks"]

# 将数据按城市分组，计算每个城市的店铺数量，并按数量降序排列
stores_by_city = starbucks.groupby("City").size().reset_index(name="NumStores")
stores_by_city = stores_by_city.sort_values(by="NumStores", ascending=False).reset_index(drop=True)

# 选择数量最多的前 10 个城市
top10_cities = stores_by_city.head(10)

# 创建柱状图
plt.figure(figsize=(10, 6))
plt.bar(top10_cities["City"], top10_cities["NumStores"])
plt.title("Top 10 Cities with Most Starbucks Stores", fontsize=16)
plt.xlabel("City", fontsize=14)
plt.ylabel("Number of Stores", fontsize=14)
plt.xticks(rotation=45, ha="right")
plt.tight_layout()
plt.show()


# In[24]:



    import pandas as pd

# 读取数据
starbucks = pd.read_csv('directory.csv')
starbucks['City'].fillna('Unknown', inplace=True)

# 将城市名中的中文汉字转换成拼音
starbucks['City'] = starbucks['City'].apply(lambda x: ''.join(lazy_pinyin(x)))

# 将数据按城市分组，计算每个城市的店铺数量，并按数量降序排列
city_counts = starbucks.groupby('City').size().reset_index(name='Counts')
city_counts.sort_values(by=['Counts'], ascending=False, inplace=True)

# 取出排名前 10 的城市
top_cities = city_counts.head(10)['City'].tolist()

print('星巴克门店数量排名前 10 的城市是：')
print(top_cities)


# In[32]:


import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif']=['SimSun']


# In[34]:


import matplotlib.pyplot as plt
import pandas as pd

# 读取数据
data = pd.read_csv('directory.csv')

# 按照城市分组统计门店数量，排序后取前10个城市
city_counts = data.groupby(['City'])['Brand'].count().sort_values(ascending=False)[:10]

# 绘制柱状图
plt.bar(city_counts.index, city_counts.values)

# 添加图表标题和坐标轴标签
plt.title('Top 10 Cities with Most Starbucks Stores in China')
plt.xlabel('City')
plt.ylabel('Number of Stores')

# 显示图像
plt.show()


# In[35]:


import matplotlib.pyplot as plt

# 经营方式及其占比
labels = ['Company Owned', 'Licensed', 'Joint Venture', 'Franchise']
sizes = [65.4, 6.8, 26.3, 1.5]

# 绘制饼状图
fig, ax = plt.subplots()
ax.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=90)
ax.axis('equal')  # 保证长宽相等，使图像更圆

# 添加图表标题
plt.title('Starbucks Store Ownership Types in China')

# 显示图像
plt.show()


# In[ ]:




