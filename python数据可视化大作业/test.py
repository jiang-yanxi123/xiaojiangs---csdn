import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm

print("1.使用Pandas库读取文件，查看原始数据的相关特征和描述信息，检查是否有空值。")
# 使用 GBK 编码方式读取文件
df = pd.read_csv("StudentsPerformance.csv", encoding="gbk")
# 查看数据的前几行
print(df.head())
# 查看数据的基本信息，包括每列数据的类型和非空数量等
print(df.info())
# 查看数据的统计信息，包括每列数据的基本统计量（如均值、标准差、最大值、最小值等）
print(df.describe())
# 检查是否有空值
print(df.isnull().sum())


print("2.总分及平均分")
# 选取阅读成绩、数学成绩、写作成绩3个字段，计算总分和平均分
df['总分'] = df.iloc[:, 5:8].sum(axis=1)
df['平均分'] = df['总分'] / 3

# 查看计算结果
print(df[['阅读成绩', '数学成绩', '写作成绩', '总分', '平均分']].head())

print("3.判断每个学生的整体状态是否通过")

# 设置及格线为60分，并使用 lambda 函数判断每位学生是否通过各门课程
df['pass_reading'] = df['阅读成绩'].apply(lambda x: 'Pass' if x >= 60 else 'Fail')
df['pass_math'] = df['数学成绩'].apply(lambda x: 'Pass' if x >= 60 else 'Fail')
df['pass_writing'] = df['写作成绩'].apply(lambda x: 'Pass' if x >= 60 else 'Fail')

# 打印修改后的数据帧
print(df.head())

print("4.判断每个学生的整体状态是否通过")
# 使用 apply() 函数和 lambda 表达式判断每个学生的整体状态是否通过，并将判断结果存储在新的数据列中
df['情况'] = df.apply(lambda x: 'Pass' if x['pass_reading'] == 'Pass' and x['pass_math'] == 'Pass' and x['pass_writing'] == 'Pass' else 'Fail', axis=1)

# 打印修改后的数据帧
print(df.head())


print("5.根据平均分设置5级制成绩")
# 计算每个学生的总分和平均分，并使用条件表达式判断成绩等级，并将判断结果存储在新的数据列中
df['总分'] = df['数学成绩'] + df['阅读成绩'] + df['写作成绩']
df['平均分'] = df['总分'] / 3
df['等级'] = df.apply(lambda x: '优秀' if x['平均分'] >= 90 else '良好' if x['平均分'] >= 80 else '中等' if x['平均分'] >= 70 else '及格' if x['平均分'] >= 60 else '不及格', axis=1)

# 打印修改后的数据帧
print(df.head())


# 指定字体
plt.rcParams['font.sans-serif'] = ['SimHei']

# 设置中文显示字体
def set_chinese_font():
    myfont = fm.FontProperties(fname=r'C:\Windows\Fonts\simhei.ttf')
    sns.set(font=myfont.get_name())
    plt.rcParams['font.sans-serif'] = [myfont.get_name()]


# 统计每个家长受教育水平的人数，并绘制水平柱状图
def plot_parent_edu_count():
    set_chinese_font()
    edu_counts = df['父母教育程度'].value_counts()
    plt.bar(edu_counts.index, edu_counts.values, color='steelblue', alpha=0.8)
    plt.xlabel('父母受教育程度')
    plt.ylabel('人数')
    plt.title('父母受教育程度的水平柱状图')

    for i, v in enumerate(edu_counts.values):
        plt.annotate(str(v), xy=(i, v), ha='center', va='bottom', fontsize=12)

    plt.title('不同父母教育程度学生人数分布柱状图', fontsize=16)
    plt.tight_layout()
    plt.show()


# 统计及格和不及格的人数，并绘制饼图
def plot_pass_fail_pie():
    set_chinese_font()
    pass_counts = df['情况'].value_counts()
    labels = ['及格', '不及格']
    sizes = [pass_counts['Pass'], pass_counts['Fail']]
    colors = ['steelblue', 'lightgrey']
    explode = (0.05, 0)

    plt.pie(sizes,
            labels=labels,
            autopct='%1.1f%%',
            colors=colors,
            explode=explode,
            textprops={'fontsize': 14})
    plt.title('全体学生成绩分布饼图', fontsize=16)
    plt.axis('equal')
    plt.show()


# 绘制数学成绩、阅读成绩和写作成绩的直方图
def plot_subject_histogram():
    set_chinese_font()
    plt.hist(df['数学成绩'], bins=10, alpha=0.5, label='数学成绩', color='steelblue')
    plt.hist(df['阅读成绩'], bins=10, alpha=0.5, label='阅读成绩', color='lightblue')
    plt.hist(df['写作成绩'], bins=10, alpha=0.5, label='写作成绩', color='lightskyblue')

    plt.legend(loc='upper right', fontsize=12)
    plt.xlabel('成绩', fontsize=12)
    plt.ylabel('人数', fontsize=12)
    plt.title('各科成绩分布直方图', fontsize=16)
    plt.tight_layout()
    plt.show()


# 绘制父母受教育程度和前置课程是否完成的分类图
def plot_parent_edu_classify():
    set_chinese_font()
    sns.countplot(x='父母教育程度',
                  data=df,
                  hue='课程完成情况',
                  palette='Set2')

    plt.title('父母受教育程度与前置课程是否完成统计分类图', fontsize=16)
    plt.xlabel('父母受教育程度', fontsize=12)
    plt.ylabel('人数', fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.show()


# 绘制成绩评级和性别分布的箱线图
def plot_score_box():
    set_chinese_font()
    sns.boxplot(x='等级',
                y='总分',
                hue='性别',
                data=df,
                palette='Set2')

    plt.title('成绩评级与性别分布箱线图', fontsize=16)
    plt.xlabel('成绩评级', fontsize=12)
    plt.ylabel('总分', fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.show()


# 绘制午餐标准和总成绩的性别分类散点图
def plot_lunch_scatter():
    set_chinese_font()
    sns.scatterplot(x='午餐',
                    y='总分',
                    hue='性别',
                    data=df,
                    color='steelblue')

    plt.title('午餐标准与总成绩的性别分类散点图', fontsize=16)
    plt.xlabel('午餐标准', fontsize=12)
    plt.ylabel('总分', fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.show()


# 计算各特征之间的相关系数，并绘制热力图
def plot_corr_heatmap():
    corr = df.corr()
    sns.heatmap(corr, annot=True, cmap='coolwarm')
    plt.title('各特征的相关热力图')
    plt.show()

# 调用绘制图表的函数
plot_parent_edu_count()
plot_pass_fail_pie()
plot_subject_histogram()
plot_parent_edu_classify()
plot_score_box()
plot_lunch_scatter()
plot_corr_heatmap()

# 将处理后的数据输出到文件，文件名为 newstudents.csv
df.to_csv('newstudents.csv', index=False)

# 读取输出的文件，检查是否正确
new_df = pd.read_csv('newstudents.csv')
print(new_df.head())

