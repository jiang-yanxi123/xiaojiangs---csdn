#include"Queue.h"
void QueueInit(Queue* pq) {
	assert(pq);
	pq->head = pq->tail = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq) {
	assert(pq);
	QNode* cur = pq->head;
	while (cur)
	{
		QNode* next = cur->next;
		free(cur);
		cur = next;
	}
	pq->head = pq->tail = NULL;
	pq->tail = 0;
}

void QueuePush(Queue* pq, QDatatype x) {//向队列尾部插入元素
	QNode* newcode = (QNode*)malloc(sizeof(QNode));
	if (newcode==NULL)
	{
		perror("malloc fail");
		return;
	}

	newcode->data = x;
	newcode->next = NULL;

	if (pq->head == NULL) {
		assert(pq->tail == NULL);
		pq->head = pq->tail = newcode;
	}
	else {
		pq->tail->next = newcode;
		pq->tail = newcode;
	}
	pq->size++;
}


void QueuePop(Queue* pq) {
	assert(pq);
	assert(pq->head != NULL);
	QNode* next = pq->head->next;
	free(pq->head);
	pq->head = next;
	if (pq->head== NULL)
		pq->tail = next;
	pq->size--;
}
//QueueInit：初始化队列
//QueueDestroy：销毁队列
//QueuePush：向队列尾部插入元素
//QueuePop：弹出队列头部元素
int  QueueSize(Queue* pq)//QueueSize：获取队列大小
{
	assert(pq);
	return pq->size;
}

bool QueueEmpty(Queue* pq)//QueueEmpty：判断队列是否为空
{
	assert(pq);
	return pq->size == 0;
}

QDatatype QueueFront(Queue* pq)//Queueont：获取队列头部元素
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->head->data;
}

QDatatype QueueBack(Queue* pq)//QueueBack：获取队列尾部元素
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->tail->data;
}