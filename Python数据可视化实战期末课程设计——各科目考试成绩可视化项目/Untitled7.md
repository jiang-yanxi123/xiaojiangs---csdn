```python
import pandas as pd

import pandas as pd

# 使用 GBK 编码方式读取文件
df = pd.read_csv("StudentsPerformance.csv", encoding="gbk")


# 查看数据的前几行
print(df.head())

# 查看数据的基本信息，包括每列数据的类型和非空数量等
print(df.info())

# 查看数据的统计信息，包括每列数据的基本统计量（如均值、标准差、最大值、最小值等）
print(df.describe())

# 检查是否有空值
print(df.isnull().sum())

```

      性别 民族 父母教育程度     午餐 课程完成情况  数学成绩  阅读成绩  写作成绩
    0  女  B   学士学位     标准    未完成    72    72    74
    1  女  C  大学未毕业     标准     完成    69    90    88
    2  女  B   硕士学位     标准    未完成    90    95    93
    3  男  A  副学士学位  自由/减少    未完成    47    57    44
    4  男  C  大学未毕业     标准    未完成    76    78    75
    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 1000 entries, 0 to 999
    Data columns (total 8 columns):
     #   Column  Non-Null Count  Dtype 
    ---  ------  --------------  ----- 
     0   性别      1000 non-null   object
     1   民族      1000 non-null   object
     2   父母教育程度  1000 non-null   object
     3   午餐      1000 non-null   object
     4   课程完成情况  1000 non-null   object
     5   数学成绩    1000 non-null   int64 
     6   阅读成绩    1000 non-null   int64 
     7   写作成绩    1000 non-null   int64 
    dtypes: int64(3), object(5)
    memory usage: 62.6+ KB
    None
                 数学成绩         阅读成绩         写作成绩
    count  1000.00000  1000.000000  1000.000000
    mean     66.08900    69.169000    68.054000
    std      15.16308    14.600192    15.195657
    min       0.00000    17.000000    10.000000
    25%      57.00000    59.000000    57.750000
    50%      66.00000    70.000000    69.000000
    75%      77.00000    79.000000    79.000000
    max     100.00000   100.000000   100.000000
    性别        0
    民族        0
    父母教育程度    0
    午餐        0
    课程完成情况    0
    数学成绩      0
    阅读成绩      0
    写作成绩      0
    dtype: int64
    


```python
# 选取阅读成绩、数学成绩、写作成绩3个字段，计算总分和平均分
df['总分'] = df.iloc[:,5:8].sum(axis=1)
df['平均分'] = df['总分'] / 3

# 查看计算结果
print(df[['阅读成绩', '数学成绩', '写作成绩', '总分', '平均分']].head())

```

       阅读成绩  数学成绩  写作成绩   总分        平均分
    0    72    72    74  218  72.666667
    1    90    69    88  247  82.333333
    2    95    90    93  278  92.666667
    3    57    47    44  148  49.333333
    4    78    76    75  229  76.333333
    


```python
# 设置及格线为60分，并使用 lambda 函数判断每位学生是否通过各门课程
df['pass_reading'] = df['阅读成绩'].apply(lambda x: 'Pass' if x >= 60 else 'Fail')
df['pass_math'] = df['数学成绩'].apply(lambda x: 'Pass' if x >= 60 else 'Fail')
df['pass_writing'] = df['写作成绩'].apply(lambda x: 'Pass' if x >= 60 else 'Fail')

# 打印修改后的数据帧
print(df.head())
```

      性别 民族 父母教育程度     午餐 课程完成情况  数学成绩  阅读成绩  写作成绩   总分        平均分 pass_reading  \
    0  女  B   学士学位     标准    未完成    72    72    74  218  72.666667         Pass   
    1  女  C  大学未毕业     标准     完成    69    90    88  247  82.333333         Pass   
    2  女  B   硕士学位     标准    未完成    90    95    93  278  92.666667         Pass   
    3  男  A  副学士学位  自由/减少    未完成    47    57    44  148  49.333333         Fail   
    4  男  C  大学未毕业     标准    未完成    76    78    75  229  76.333333         Pass   
    
      pass_math pass_writing  
    0      Pass         Pass  
    1      Pass         Pass  
    2      Pass         Pass  
    3      Fail         Fail  
    4      Pass         Pass  
    


```python
# 使用 apply() 函数和 lambda 表达式判断每个学生的整体状态是否通过，并将判断结果存储在新的数据列中
df['情况'] = df.apply(lambda x: 'Pass' if x['pass_reading'] == 'Pass' and x['pass_math'] == 'Pass' and x['pass_writing'] == 'Pass' else 'Fail', axis=1)

# 打印修改后的数据帧
print(df.head())
```

      性别 民族 父母教育程度     午餐 课程完成情况  数学成绩  阅读成绩  写作成绩   总分        平均分 pass_reading  \
    0  女  B   学士学位     标准    未完成    72    72    74  218  72.666667         Pass   
    1  女  C  大学未毕业     标准     完成    69    90    88  247  82.333333         Pass   
    2  女  B   硕士学位     标准    未完成    90    95    93  278  92.666667         Pass   
    3  男  A  副学士学位  自由/减少    未完成    47    57    44  148  49.333333         Fail   
    4  男  C  大学未毕业     标准    未完成    76    78    75  229  76.333333         Pass   
    
      pass_math pass_writing    情况  
    0      Pass         Pass  Pass  
    1      Pass         Pass  Pass  
    2      Pass         Pass  Pass  
    3      Fail         Fail  Fail  
    4      Pass         Pass  Pass  
    


```python
# 计算每个学生的总分和平均分，并使用条件表达式判断成绩等级，并将判断结果存储在新的数据列中
df['总分'] = df['数学成绩'] + df['阅读成绩'] + df['写作成绩']
df['平均分'] = df['总分'] / 3
df['等级'] = df.apply(lambda x: '优秀' if x['平均分'] >= 90 else '良好' if x['平均分'] >= 80 else '中等' if x['平均分'] >= 70 else '及格' if x['平均分'] >= 60 else '不及格', axis=1)

# 打印修改后的数据帧
print(df.head())
```

      性别 民族 父母教育程度     午餐 课程完成情况  数学成绩  阅读成绩  写作成绩   总分        平均分 pass_reading  \
    0  女  B   学士学位     标准    未完成    72    72    74  218  72.666667         Pass   
    1  女  C  大学未毕业     标准     完成    69    90    88  247  82.333333         Pass   
    2  女  B   硕士学位     标准    未完成    90    95    93  278  92.666667         Pass   
    3  男  A  副学士学位  自由/减少    未完成    47    57    44  148  49.333333         Fail   
    4  男  C  大学未毕业     标准    未完成    76    78    75  229  76.333333         Pass   
    
      pass_math pass_writing    情况   等级  
    0      Pass         Pass  Pass   中等  
    1      Pass         Pass  Pass   良好  
    2      Pass         Pass  Pass   优秀  
    3      Fail         Fail  Fail  不及格  
    4      Pass         Pass  Pass   中等  
    


```python
import seaborn as sns
import matplotlib.pyplot as plt
# 指定字体
plt.rcParams['font.sans-serif'] = ['SimHei']
# 统计每个家长受教育水平的人数，并绘制水平柱状图
edu_counts = df['父母教育程度'].value_counts()
plt.bar(edu_counts.index, edu_counts.values)
plt.xlabel('父母教育程度')
plt.ylabel('人数')
plt.title('父母受教育程度的水平柱状图')
plt.show()

# 统计及格和不及格的人数，并绘制饼图
pass_counts = df['情况'].value_counts()

labels = ['及格', '不及格']
sizes = [pass_counts['Pass'], pass_counts['Fail']]

plt.pie(sizes,
        labels=labels,
        autopct='%1.1f%%')

plt.title('全体学生成绩分布饼图')
plt.axis('equal')
plt.show()

# 绘制数学成绩、阅读成绩和写作成绩的直方图
plt.hist(df['数学成绩'], bins=10, alpha=0.5, label='math score')
plt.hist(df['阅读成绩'], bins=10, alpha=0.5, label='reading score')
plt.hist(df['写作成绩'], bins=10, alpha=0.5, label='writing score')

plt.legend(loc='upper right')
plt.xlabel('成绩')
plt.ylabel('人数')
plt.title('各科成绩分布直方图')
plt.show()

# 绘制父母受教育程度和前置课程是否完成的分类图
sns.countplot(x='父母教育程度',
              data=df,
              hue='课程完成情况')
plt.title('父母受教育程度与前置课程是否完成统计分类图')
plt.show()

# 绘制成绩评级和性别分布的箱线图
sns.boxplot(x='等级',
            y='总分',
            hue='性别',
            data=df)
plt.title('成绩评级与性别分布箱线图')
plt.show()

# 绘制午餐标准和总成绩的性别分类散点图
sns.scatterplot(x='午餐',
                y='总分',
                hue='性别',
                data=df)
plt.title('午餐标准与总成绩的性别分类散点图')
plt.show()

# 计算各特征之间的相关系数，并绘制热力图
corr = df.corr()
sns.heatmap(corr, annot=True, cmap='coolwarm')
plt.title('各特征的相关热力图')
plt.show()
```


    
![png](output_5_0.png)
    



    
![png](output_5_1.png)
    



    
![png](output_5_2.png)
    



    
![png](output_5_3.png)
    



    
![png](output_5_4.png)
    



    
![png](output_5_5.png)
    



    
![png](output_5_6.png)
    



```python

```
