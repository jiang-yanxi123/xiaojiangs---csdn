#!/usr/bin/env python
# coding: utf-8

# In[1]:


# 打开文件并读取前30个字符
with open('作业.txt', 'r') as f:
    first_30_chars = f.read(30)
    print(first_30_chars)

# 打印文件前3行内容
with open('作业.txt', 'r') as f:
    first_3_lines = []
    for i in range(3):
        line = f.readline()
        first_3_lines.append(line)
    print(''.join(first_3_lines))


# In[4]:


# 读取作业.txt的内容
with open('作业.txt', 'r', encoding='utf-8') as f:
    content = f.read()

# 在当前文件路径下创建一个新文件"木兰辞.txt"
with open('木兰辞.txt', 'w', encoding='utf-8') as f:
    # 在新文件的第一行添加“木兰辞”为首行
    f.write("木兰辞\n")
    # 将作业.txt中所有内容写入该文档
    f.write(content)


# In[ ]:




