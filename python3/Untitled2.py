#!/usr/bin/env python
# coding: utf-8

# In[1]:


from PIL import Image, ImageDraw

# 打开校园图片
with Image.open("校园.jpg") as im:
    # 显示原始图片的尺寸
    print("原始图片尺寸:", im.size)
    
    # 缩小图片并保存为“校园1.jpg”
    new_size = (im.width//2, im.height//2)
    im.thumbnail(new_size)
    im.save("校园1.jpg")
    print("缩小后的图片尺寸:", im.size)


# In[2]:


# 旋转图片并保存为“校园2.jpg”
rotated_im = im.rotate(180)
rotated_im.save('校园2.jpg')


# In[3]:


# 获取红色和蓝色通道，并将它们交换，保存为“校园3.jpg”
r, g, b = im.split()
new_im = Image.merge("RGB", (b, g, r))
new_im.save("校园3.jpg")


# In[4]:


# 获取图片轮廓并保存为“校园4.jpg”
draw = ImageDraw.Draw(im)
draw.rectangle((0, 0, im.width-1, im.height-1), outline="red")
del draw
im.save("校园4.jpg")


# In[ ]:




