#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//#include <stdlib.h>
//#include <stdbool.h>
//
//// 冒泡排序函数
//void BubbleSort(int arr[], int n) {
//    for (int i = 0; i < n - 1; ++i) {
//        bool flag = false;  // 标志位，用于判断此轮是否发生了交换
//        for (int j = 0; j < n - i - 1; ++j) {
//            if (arr[j] > arr[j + 1]) {  // 相邻元素比较
//                int tmp = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tmp;
//                flag = true;  // 发生交换，修改标志位
//            }
//        }
//        if (!flag) {  // 如果此轮没有发生交换，说明已经有序，直接退出循环
//            break;
//        }
//    }
//}
//
//
//
//int main() {
//    int n;               // 数组长度
//    int* arr = NULL;     // 动态分配内存的数组
//    const char* file = "input.txt";  // 输入文件名
//    FILE* fin = fopen(file, "r");
//    if (fin == NULL) {   // 文件打开失败，报错并退出程序
//        perror("fopen error");
//        return -1;
//    }
//    if (fscanf(fin, "%d", &n) != 1) {  // 读取数据长度失败，报错并退出程序
//        fprintf(stderr, "Read data length error\n");
//        fclose(fin);
//        return -1;
//    }
//    if (n <= 0) {   // 数据长度非法，报错并退出程序
//        fprintf(stderr, "Data length invalid\n");
//        fclose(fin);
//        return -1;
//    }
//    arr = (int*)malloc(sizeof(int) * n);  // 分配内存
//    if (arr == NULL) {  // 内存分配失败，报错并退出程序
//        perror("malloc error");
//        fclose(fin);
//        return -1;
//    }
//    for (int i = 0; i < n; ++i) {  // 读入数据
//        if (fscanf(fin, "%d", &arr[i]) != 1) {  // 读取数据错误，报错并退出程序
//            fprintf(stderr, "Read data error\n");
//            fclose(fin);
//            free(arr);  // 释放已经分配的内存
//            return -1;
//        }
//    }
//    fclose(fin);  // 关闭输入文件
//    BubbleSort(arr, n);  // 调用冒泡排序函数进行排序
//    const char* out_file = "output.txt";  // 输出文件名
//    FILE* fout = fopen(out_file, "w");   // 打开输出文件
//    if (fout == NULL) {  // 文件打开失败，报错并退出程序
//        perror("fopen error");
//        free(arr);  // 释放已经分配的内存
//        return -1;
//    }
//    fprintf(fout, "%d\n", n);  // 输出排序后的数据
//    for (int i = 0; i < n; ++i) {
//        fprintf(fout, "%d ", arr[i]);
//    }
//    fprintf(fout, "\n");
//    fclose(fout);  // 关闭输出文件
//    free(arr);     // 释放已经分配的内存
//    return 0;
//}
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<math.h>
typedef int BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;

BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node==NULL)
	{
		perror("node fail");
		return;
	}
	node->data = x;
	node->left = NULL;
	node->right = NULL;
	return node;
}

BTNode* CreatTree()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);
	node1->left  = node2;
	node1->right = node4;
	node2->left = node3;
	node4->left = node5;
	node4->right = node6;

	return node1;
}

void PreOrder(BTNode* root)
{
	if (root==NULL)
	{
		printf("NULL "); 
		return 0;
	}
	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

void lnOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return 0;
	}
	lnOrder(root->left);
	printf("%d ", root->data);
	lnOrder(root->right);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return 0;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//int size = 0;
//int TreeSize(BTNode* root) {
//	if (root == NULL)
//		return;
//	++size;
//	TreeSize(root->left);
//	TreeSize(root->right);
//	return size;
//}
//最简便的做法
int TreeSize(BTNode* root)
{
	return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
}//指挥打工人

int TreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	int LeftHeight = TreeHeight(root->left);
	int RightHeight = TreeHeight(root->right);
	/*return root = max(LeftHeight + 1, RightHeight + 1);*/
	return LeftHeight > RightHeight ? LeftHeight +1: RightHeight + 1;
}

int TreeKLevel(BTNode* root,int k)
{
	assert(k > 0);
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	int leftK  = TreeKLevel(root->left, k-1);
	int rightK = TreeKLevel(root->right,k-1);
	return leftK + rightK;
}

BTNode* BinaryTreeFind(BTNode* root, BTDataType x) 
{
	if (root == NULL)
		return NULL;
	if (root->data==x)
		return  root;//直接返回地址
	//if(root->left!=0|| root->right!=0)
	//	return BinaryTreeFind(root->left,x)|| BinaryTreeFind(root->right,x);
	//return NULL;
	BTNode* lret = BinaryTreeFind(root->left, x);
	if (lret)
		return  lret;
	BTNode* rret = BinaryTreeFind(root->right, x);
	if (rret)
		return rret;
	return NULL;
}


int main()
{
	BTNode* root = CreatTree();
	PreOrder(root);//前序遍历
	printf("\n");
	lnOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");
	//size = 0;
	int k = 0;
	printf("请输入，你要查看二叉树第几层的节点数？->>>");
	scanf("%d", &k);
	int size=TreeSize(root);
	printf("TreeSize:%d\n", size);
	int Height = TreeHeight(root);
	printf("TreeHeight:%d\n", Height);
	int KLevel = TreeKLevel(root,k);
	printf("TreeKLevel:%d\n", KLevel);
	int m = 0;
	printf("请输入，你要查找的节点值？->>>");
	scanf("%d", &m);
	int n= BinaryTreeFind(root, m);
	printf("节点为:%d\n", n);
	return 0;
}