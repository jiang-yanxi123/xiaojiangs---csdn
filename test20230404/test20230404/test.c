#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"
//int main()
//{
//	HP hp;
//	HeapInit(&hp);
//	HeapPush(&hp,12);
//	HeapPush(&hp,4);
//	HeapPush(&hp,13);
//	HeapPush(&hp,5);
//	HeapPush(&hp,66);
//	HeapPush(&hp,7);
//	HeapPush(&hp,45);
//	HeapPush(&hp,5);
//	HeapPush(&hp,33);
//	HeapPush(&hp,35);
//	HeapPush(&hp,44);
//	HeapPush(&hp,8);
//	int k = 0;
//	scanf("%d",&k);//取堆的前几个数！
//	while (!HeapEmpty(&hp)&&k--){
//		printf("%d ", HeapTop(&hp));
//		HeapPop(&hp);
//	}
//	printf("\n");
//	return 0;
//}
//排升序--建小堆 
void HeapSort1(int*a,int n)
{	
	//建堆，向上调整
	for(int i = 1; i < n; ++i) 
	{
		AdjustUP(a,i);
	}
	// 交换堆顶元素和最后一个元素，并调整堆
	int end = n - 1;
	while (end > 0) {
		Swap(&a[end], &a[0]);
		AdjustDown(a,end,0);
		--end;
	}
}
// 排降序——建大推//建堆的时间复杂度为 O(n)，排序的时间复杂度为 O(n log n)。因此，堆排序的总时间复杂度为 O(n log n)
void HeapSort2(int* a, int n)
{
	//建堆--向下调整建堆
	for (int i = (n - 1 - 1) /2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}
	// 交换堆顶元素和最后一个元素，并调整堆
	int end = n - 1;
	while (end > 0) {
		Swap(&a[end], &a[0]);
		AdjustDown(a, end, 0);
		--end;
	}

}

void PrintTopk(const char* file, int k) {
	//1.建堆，用a中前K个元素建堆
	int* topk = (int*)malloc(sizeof(int) * k);
	if (topk== NULL)
	{
		perror("topk error");
		return;
	}
	FILE* fout = fopen(file, "r");
	if (fout == NULL)
	{
		perror("fopen error");
		return;
	}
	//读出前K个数据建小堆
	for (int i = 0; i < k; i++) {
		fscanf(fout, "%d", &topk[i]);
	}
	for (int i = (k - 2) / 2; i >= 0; --i)
	{
		AdjustDown(topk, k, i);
	}
	//2.将剩余N-k个元素依次与堆顶元素交换，不满则替换；
	int val = 0;
	int ret = fscanf(fout, "%d", &val);
	while (ret != EOF)
	{
		if (val>topk[0])
		{
			topk[0] = val;
			AdjustDown(topk, k, 0);
		}
		ret = fscanf(fout, "%d", &val);
	}
	for (int i = 0; i < k; i++)
	{
		printf("%d ", topk[i]);
	}
	printf("\n");
	free(topk);
	fclose(fout);
}
void CreateNDateTopk() {
	//造数据
	int  n = 10000;
	srand(time(0));
	const  char* file = "data.txt";
	FILE* fin = fopen(file,"w");
	if (fin == NULL)
	{
		perror("fopen error");
		return;
	}
	for (size_t i = 0; i < n; i++)
	{
		int x = rand() % 10000;
		fprintf(fin, "%d\n", x);
	}
	fclose(fin);
	//PrintTopk(10);
	PrintTopk(file, 10);
}
int main() {
	//CreateNDate();
	PrintTopk("data.txt", 10);
	return 0;
}
//int  main() {
//	int n;
//	printf("请输入学生数：");
//	scanf("%d", &n);
//	int* arr = (int*)malloc(n * sizeof(int)); // 动态分配数组内存空间
//	if (arr ==NULL) {
//		perror("realloc fail");
//		return -1;
//	}
//	printf("请输入学生的分数\n");
//	for (int i = 0; i <n; i++) {
//		printf("输入学生%d的分数", i+1);
//		scanf("%d", &arr[i]);
//	}
//	//int a[10] = { 1,2,8,9,4,5,15,16,19,10 };
//	//int n = sizeof(a) / sizeof(a[0]);
//	HeapSort2(arr, n);
//	printf("分数排序是");
//	for (int i = 0; i < n; ++i) {
//		printf("%d ", arr[i]);
//	}
//	free(arr);
//	return 0;
//}

