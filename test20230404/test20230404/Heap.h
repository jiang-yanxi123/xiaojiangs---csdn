#pragma once
typedef int HPDataType;
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>

typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;


void HeapInit(HP* php);
void HeapInitArray(HP* php, int* a, int n);
void HeapPush(HP* php,HPDataType x);
void HeapPop(HP* php);

HPDataType HeapTop(HP* php);
bool HeapEmpty(HP* php);
int HeapSize(HP* php);

void AdjustUP(HPDataType* a, int child);
void AdjustDown(HPDataType* a, int n, int parent);

void HeapDestroy(HP* php);
void Swap(HPDataType* p1, HPDataType* p2);