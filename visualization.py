# 导入相应模块
import pandas as pd
import matplotlib.pyplot as plt

# 导入数据并显示前5行
tips_data = pd.read_excel('tips.xls')
print(tips_data.head())

# 导入数据并显示描述信息
print(tips_data.describe())

# 修改列名并显示前5行
tips_data.columns = ['消费总额', '小费', '性别', '是否抽烟', '星期', '聚餐时间段', '人数']
print(tips_data.head())


# 导入数据并增加“人均消费”列
tips_data['人均消费'] = tips_data['消费总额'] / tips_data['人数']
print(tips_data.head())



# 导入数据并查询抽烟男性中人均消费大于5的数据
smoking_male = tips_data[(tips_data['是否抽烟']=='Yes') & (tips_data['性别']=='Male')]
result = smoking_male[smoking_male['消费总额'] / smoking_male['人数'] > 5]
print(result)


# 导入数据并绘制散点图
x = tips_data['消费总额']
y = tips_data['小费']
plt.scatter(x, y)
plt.xlabel('Total bill')
plt.ylabel('Tip')
plt.show()


# 导入数据并计算男女顾客的小费平均值
gender_tip_mean = tips_data.groupby('性别')['小费'].mean()
print(gender_tip_mean)


# 导入数据并绘制直方图
grouped = tips_data.groupby('星期')['小费']
hist_data = [grouped.get_group(day) for day in grouped.groups]
plt.hist(hist_data, bins=10, histtype='bar', stacked=True)
plt.legend(grouped.groups.keys())
plt.xlabel('Tip amount')
plt.ylabel('Frequency')
plt.show()

# 导入数据并绘制箱线图
fig, ax = plt.subplots()
ax.boxplot([tips_data[tips_data['性别']=='Male'][tips_data['是否抽烟']=='Yes']['小费'],
            tips_data[tips_data['性别']=='Male'][tips_data['是否抽烟']=='No']['小费'],
            tips_data[tips_data['性别']=='Female'][tips_data['是否抽烟']=='Yes']['小费'],
            tips_data[tips_data['性别']=='Female'][tips_data['是否抽烟']=='No']['小费']],
           labels=['Male smoker', 'Male non-smoker', 'Female smoker', 'Female non-smoker'])
plt.xlabel('Gender and smoking')
plt.ylabel('Tip amount')
plt.title('Effect of gender and smoking on tipping behavior')
plt.show()

# 导入相应模块
import pandas as pd
import matplotlib.pyplot as plt

# 导入数据并绘制散点图
colors = ['blue', 'green', 'red', 'purple']
grouped = tips_data.groupby('聚餐时间段')
for i, (key, group) in enumerate(grouped):
    plt.scatter(group['消费总额'], group['小费'], label=key, color=colors[i])
plt.xlabel('Total bill amount')
plt.ylabel('Tip amount')
plt.title('Relationship between meal time and tipping behavior')
plt.legend()
plt.show()

